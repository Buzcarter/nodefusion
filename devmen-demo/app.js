// ------------------------------
// require
// ------------------------------
var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
// var favicon = require('favicon');

// ------------------------------
// begin assembling the app
// ------------------------------
var app = express();

// configure the application
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));

// ------------------------------
// use middleware
// ------------------------------
// NOTE: the order of includes is important
// So let's do statics first:
app.use(express.static(path.join(__dirname, 'bower_components/bootstrap/dist')));
app.use(express.static(path.join(__dirname, 'bower_components/jquery/dist')));

// app.use(favicon());

app.use(bodyParser.urlencoded({
	extended: false
}));

// parse application/json
// ------------------------------
app.use(bodyParser.json());

app.use(function(err, req, res, next) {
	// doesn't seem to handle 404?
	res.status(err.status || 500);
	res.render('error', {
		message: err.message,
		error: err
	});
});

// ------------------------------
// define our routes
// ------------------------------
// using intermediate variable just to be clear
var my_routes = require('./routes/routes.js');
app.use(my_routes);

// ------------------------------
// boot the server
// ------------------------------

// grab port from environment if deployed in production:
var port = process.env.PORT || 9000;
app.listen(port, function() {
	console.log('ready. listening on ' + port);
});