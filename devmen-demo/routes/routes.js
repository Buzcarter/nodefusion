var express = require('express');
var routes = express();

var todo_items = [{
	id: 0,
	title: 'This American Life'
}, {
	id: 1,
	title: 'Rise Again'
}, {
	id: 2,
	title: 'Foo'
}];

routes.get('/', function(req, res) {
	res.send('Hello, express!');
});

routes.get('/book/:title', function(req, res) {
	res.render('books', {
		title: req.params.title,
		items: todo_items
	});
});

routes.post('/add', function(req, res) {
	var item = req.body.new_item;
	todo_items.push({
		id: todo_items.length,
		title: item
	});
	res.redirect('/book/added');
});

module.exports = routes;