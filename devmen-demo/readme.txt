# let's globally install install express so it's available at command line from anywhere:

npm install -g express
npm install -g express-generator

# npm install nodemon

# npm install hjs -c less
# ejs -> embedded JavaScript
npm i ejs
npm i body-parser

# add bower to my path; bower is npm-like, but it's for client front-end JS & CSS
npm i -g bower

bower i bootstrap

# complicated:
# http://passportjs.org/

# generate the package.json:
npm init

# http://12factor.net/

# "platform as a service" providers (PaaS):
# Heroku or Windows Azure

# hide Node behind NginX "reverse proxy"

# ddd and layered principles required - hexagonal architecture from coburn or polermo

#
