var express = require('express');
var router = express.Router();

/* GET home page. */
// router.get('/', function(req, res) {
//   res.render('index', { title: 'Express' });
// });

router.get('/', function(req, res) {
	res.render('index', {
		title: 'Pillow: the ennui social network',
		age: 500
	});
});

router.get('/users/:id', function(req, res) {
	console.log(req.params);
	res.send(200);
});

module.exports = router;