express tut102 --hogan -c less

install dependencies:
  $ cd tut102 && npm install

run the app:
  $ DEBUG=tut102 ./bin/www

# that's pretty annoying:
npm install -g nodemon

# let's attach it:
nodemon bin/www

